#include "pachete.hpp"
#include <getopt.h>
int
main(int argc, char* argv[])
{
  std::ostringstream help;
  help << "Apelare de tipul:\n" << argv[0]
       << " -t arp -m 1000 -s 192.168.1.1 -d 192.168.1.10 -i eth0\n" << argv[0]
       << " --type udp --time 1000 --src 192.168.1.5 --dst "
          "192.168.1.10 --interface eth0\n";
  if (argc == 1) {
    std::cerr << help.str();
    return (EXIT_FAILURE);
  }

  try {

    int opt;
    pachete::ABSpachet* p1 = nullptr;
    std::string ipSrc, ipDst, ifName, mili, tip;

    mili = "1000";
    ifName = pachete::ABSpachet::printDefaultInterface();

    const struct option long_options[] = {
      { "type", required_argument, 0, 't' },
      { "src", required_argument, 0, 's' },
      { "dst", required_argument, 0, 'd' },
      { "time", required_argument, 0, 'm' },
      { "interface", required_argument, 0, 'i' },
      { "help", no_argument, 0, 'h' },
      { 0, 0, 0, 0 }
    };
    while ((opt = getopt_long(argc, argv, "t:s:d:i:m:h", long_options,
                              nullptr)) != -1) {
      switch (opt) {
        case 't':
          if (strncasecmp(optarg, "arp", strlen("arp")) == 0) {
            tip = "arp";
          } else if (strncasecmp(optarg, "udp", strlen("udp")) == 0) {
            tip = "udp";
          } else if (strncasecmp(optarg, "icmp", strlen("icmp")) == 0 ||
                     strncasecmp(optarg, "ping", strlen("ping")) == 0) {
            tip = "icmp";
          } else {
            pachete::ABSpachet::printErr("Tip pachet Invalid",
                                         __PRETTY_FUNCTION__, __LINE__);
            return (EXIT_FAILURE);
          }
          break;
        case 's':
          if (optarg != nullptr)
            ipSrc = optarg;
          break;
        case 'd':
          if (optarg != nullptr)
            ipDst = optarg;
          break;
        case 'i':
          if (optarg != nullptr) {
            ifName = optarg;
          }
          break;
        case 'm':
          if (optarg != nullptr)
            mili = optarg;
          break;
        case 'h':
        case '?':
        default:
          std::cerr << help.str();
          return (EXIT_FAILURE);
      }
    }

    if (tip == "arp") {
      p1 = new pachete::ARP(ipSrc.c_str(), ipDst.c_str(), ifName.c_str());
    } else if (tip == "udp") {
      p1 = new pachete::UDP(ipSrc.c_str(), ipDst.c_str());
    } else if (tip == "icmp") {
      p1 = new pachete::ICMP(ipSrc.c_str(), ipDst.c_str());
    } else {

      pachete::ABSpachet::printErr("Tip pachet Invalid sau Nespecificat",
                                   __PRETTY_FUNCTION__, __LINE__);
      return (EXIT_FAILURE);
    }

    if (p1 != nullptr) {
      p1->Flood(std::stoul(mili, nullptr, 10));
    }

    return (EXIT_SUCCESS);

  } catch (const char* err) {
    pachete::ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    return (EXIT_FAILURE);
  }
}
