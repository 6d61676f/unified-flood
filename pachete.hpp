#ifndef PACHETE_HPP
#define PACHETE_HPP
#include <arpa/inet.h>
#include <chrono>
#include <cstdbool>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <ifaddrs.h>
#include <iostream>
#include <linux/if_packet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <random>
#include <sstream>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
namespace pachete {

struct arpHDR_t
{
  uint16_t HTYPE; // Hardware Type
  uint16_t PTYPE; // Protocol Type
  uint8_t HLEN;   // Hardware Address Length
  uint8_t PLEN;   // Protocol Address Length
  uint16_t OPER;  // Operation
  uint8_t SHA[6]; // Sender Hardware Address
  uint8_t SPA[4]; // Sender Protocol Address
  uint8_t THA[6]; // Target Hardware Address
  uint8_t TPA[4]; // Target Protocol Address
};
struct eth2HDR_t
{
  uint8_t THA[6];
  uint8_t SHA[6];
  uint16_t TYPE;
};

struct arpRAW_t
{
  struct eth2HDR_t eth2;
  struct arpHDR_t arp;
};
class ABSpachet
{
public:
  virtual bool setIp(const char ipSrc[16], const char ipDst[16]) = 0;
  virtual void Flood(uint32_t milisecunde = 1000) = 0;
  virtual bool setIpSrc(const char ipSrc[16]) = 0;
  virtual bool setIpDst(const char ipDst[16]) = 0;
  static std::string printDefaultInterface(void);
  static void printErr(const char* mesaj, const char* functie, int linie);
  virtual ~ABSpachet();

protected:
  virtual void initHeaders(void) = 0;
  virtual bool send(void) = 0;
  virtual bool check(void) = 0;
  uint16_t checksum(uint8_t* ptr, size_t bytes);
};
class UDP : public ABSpachet
{
private:
  void initHeaders(void);
  struct sockaddr_in adresa;
  struct udphdr* udpHDR = nullptr;
  struct iphdr* ipHDR = nullptr;
  const char* pachet = "W0l0l0 Fraere! h4ck";
  const size_t lungimeIP = sizeof(struct iphdr);
  const size_t lungimeUDP = sizeof(struct udphdr);
  const size_t lungimePayload = strlen(pachet) + 1;
  const size_t lungimePachet = lungimeIP + lungimeUDP + lungimePayload;
  uint8_t* head = nullptr;
  bool srcOK = false;
  bool dstOK = false;
  int socketFD;
  bool send(void);
  bool check(void);

public:
  UDP(const char* ipSRC, const char* ipDST);
  UDP(void);
  void Flood(uint32_t milisecunde = 1000);
  bool setIp(const char ipSrc[16], const char ipDst[16]);
  bool setIpSrc(const char ipSrc[16]);
  bool setIpDst(const char ipDst[16]);
  virtual ~UDP();
};
class ARP : public ABSpachet
{
public:
  ARP(void);
  ARP(const char ipT[16], const char ipG[16], const char Interfata[IFNAMSIZ]);
  virtual ~ARP();
  bool setInterfata(const char interfata[IFNAMSIZ]);
  void printARP(void);
  static const uint16_t HTYPE = 1;
  static const uint16_t PTYPE = 0x0800;
  static const uint8_t HLEN = 6;
  static const uint8_t PLEN = 4;
  static const uint16_t OPER_REPLY = 2;
  static const uint16_t OPER_REQUEST = 1;
  static const uint16_t TYPE = 0x0806;
  bool setIp(const char ipSrc[16], const char ipDst[16]);
  void Flood(uint32_t milisecunde = 1000);
  bool setIpSrc(const char ipSrc[16]);
  bool setIpDst(const char ipDst[16]);

private:
  bool send(void);
  void initHeaders(void);
  bool check(void);
  bool srcOK = false;
  bool dstOK = false;
  bool ifNameOK = false;
  int getMac(const char interfata[IFNAMSIZ]);
  struct ifreq ifreqInterfata;
  arpHDR_t* arpHDR = nullptr;
  eth2HDR_t* eth2HDR = nullptr;
  arpRAW_t arpRAW;
  struct sockaddr_ll sockLL;
  char numeInterfata[IFNAMSIZ];
  int sofd;
};
class ICMP : public ABSpachet
{
public:
  ICMP(const char ipSrc[16], const char ipDst[16]);
  ICMP(void);
  virtual ~ICMP();
  bool setIpSrc(const char ipSrc[16]);
  bool setIpDst(const char ipDst[16]);
  bool setIp(const char ipSrc[16], const char ipDst[16]);
  void Flood(uint32_t milisecunde = 1000);

private:
  bool check(void);
  bool send(void);
  void initHeaders(void);
  int socketFD;
  size_t packetSize;
  struct sockaddr_in sock;
  uint8_t* packet = nullptr;
  struct iphdr* ipHeader = nullptr;
  struct icmphdr* icmpHeader = nullptr;
  bool srcOK = false;
  bool dstOK = false;
};
}
#endif // PACHETE_HPP
