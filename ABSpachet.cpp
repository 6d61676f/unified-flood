#include "pachete.hpp"
namespace pachete {

// http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html
uint16_t
ABSpachet::checksum(uint8_t* ptr, size_t bytes)
{ // Initialise the accumulator.
  uint32_t acc = 0xffff;

  // Handle complete 16-bit blocks.
  for (size_t i = 0; i + 1 < bytes; i += 2) {
    uint16_t word;
    memcpy(&word, ptr + i, 2);
    acc += ntohs(word);
    if (acc > 0xffff) {
      acc -= 0xffff;
    }
  }

  // Handle any partial block at the end of the data.
  if (bytes & 1) {
    uint16_t word = 0;
    memcpy(&word, ptr + bytes - 1, 1);
    acc += ntohs(word);
    if (acc > 0xffff) {
      acc -= 0xffff;
    }
  }

  // Return the checksum in network byte order.
  return (htons(~acc));
}

std::string
ABSpachet::printDefaultInterface()
{
  struct ifaddrs *adrese, *next;
  std::ostringstream mesaj;
  std::string numeIf = "none";

  if (getifaddrs(&adrese) < 0) {
    std::cerr << "Eroare la getifaddrs" << std::endl;
    return numeIf;
  }

  unsigned masca = IFF_BROADCAST | IFF_RUNNING | IFF_UP | IFF_MULTICAST;

  for (next = adrese; next != nullptr; next = next->ifa_next) {
    if (next->ifa_addr->sa_family == AF_INET) {

      if ((next->ifa_flags & masca) == masca) {

        struct sockaddr_in* ip =
          reinterpret_cast<struct sockaddr_in*>(next->ifa_addr);
        struct sockaddr_in* broad =
          reinterpret_cast<struct sockaddr_in*>(next->ifa_ifu.ifu_broadaddr);
        mesaj << std::endl
              << "Nume Interfata Default: " << next->ifa_name << std::endl;
        mesaj << "Adresa IPv4: " << inet_ntoa(ip->sin_addr) << std::endl;
        mesaj << "Adresa Broadcast: " << inet_ntoa(broad->sin_addr) << std::endl
              << std::endl;
        numeIf = next->ifa_name;
        break;
      }
    }
  }
  std::cout << mesaj.str();
  freeifaddrs(adrese);
  return (numeIf);
}
void
ABSpachet::printErr(const char* mesaj, const char* functie, int linie)
{
  std::cerr << "\n"
            << "In functia:"
            << "\'" << functie << "\'\n"
            << "La linia:"
            << "\'" << linie << "\'\n"
            << "Eroarea:"
            << "\'" << mesaj << "\'\n";
}
ABSpachet::~ABSpachet()
{
}
}
